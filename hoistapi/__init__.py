"""Model exports the client for Hoist

Typical usage is

h = Hoist("MyAPIKEY")
h.login("username", "password")
votes = h["votes"]
allvotes = votes.get_objects()
onevote = votes["myid"]

"""
from hoistapi import data, auth, notify

class Hoist(object):
    """The hoist client object it is used to hold state
    such as the loged in user and current bucket
    """

    def __init__(self, api_key):
        self.api_key = api_key
        self.auth = auth.Auth(api_key)

    def login(self, username, password):
        user = self.auth.login(username, password)
        return user

    def status(self):
        return self.auth.status()

    def logout(self):
        return self.auth.logout()

    def create_user(self, username, password):
        return self.auth.create(username, password)

    def notify(self, notification_name, parameters):
        return notify.send(self, notification_name, parameters)

    def set_bucket(self, bucket_name):
        raise Exception("Not Implemented")

    def set_bucket_data(self, bucket_name, data):
        raise Exception("Not Implemented")

    def save_file(self, key, file_or_path):
        raise Exception("Not Implemented")

    def load_file(self, key, file_or_path):
        raise Exception("Not Implmented")

    #data model interface
    def __getitem__(self, model_name):
        return data.HoistModel(self, model_name)

#    def __setitem__(self, model_name):
#        raise Exception("Not Implemented")
