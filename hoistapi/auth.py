"""Auth

Create
Login
Status
Logout
"""
import urlparse
from hoistapi import util

AUTH_URL = "https://auth.hoi.io/"

def http_error_to_exception(http_error):
    """Turn different http error codes into AuthExceptions"""
    return AuthException(http_error)

class AuthException(Exception):
    """Encapsulated the errors that can come back from the data api
    usually in HTTP form
    """

    def __init__(self, base_exception):
        Exception.__init__(self)
        self.base_exception = base_exception

class Auth(object):
    """Creates an object that will handle the auth requests for you.

    class auth.Auth(api_key)
    Returns a new instance of Auth using the supplied api_key

    """

    def __init__(self, api_key):
        self.api_key = api_key
        self.auth_cookie = None

    def create(self, email, password):
        """Create a new user"""
        return self.call_auth_url("user", {"email":email, "password":password})

    def login(self, username, password):
        """Login as new user"""
        return self.call_auth_url("login",
                                  {"username":username, "password":password})

    def status(self):
        """Return the status of the user session"""
        return self.call_auth_url("status")

    def logout(self):
        """Logout and delete the user session"""
        obj = self.call_auth_url("logout", method="POST")
        self.auth_cookie = None
        return obj

    def call_auth_url(self, end_point, data=None, method=None):
        """Internal use function to call auth urls"""
        url = urlparse.urljoin(AUTH_URL, "/" + end_point)
        obj, cookie = util.loadUrlWithCookies(url,
                                              self.api_key,
                                              data,
                                              self.auth_cookie,
                                              http_error_to_exception,
                                              method)
        if cookie:
            self.auth_cookie = cookie.split(";")[0]
            print self.auth_cookie
        return obj
