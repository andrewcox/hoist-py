"""Notifiy

Send

"""

import urlparse
import urllib2
from hoistapi import util

NOTIFY_URL = "https://notify.hoi.io/"

def http_error_to_exception(http_error):
    """Helper function that turns http errors into real exceptions"""
    if not isinstance(http_error, urllib2.HTTPError):
        return UnknownNotificationException(http_error)
    elif http_error.code == 401:
        return UnauthorizedNotificationException(http_error)
    elif http_error.code == 404:
        return NotificationNotFoundException(http_error)
    else:
        return http_error

class NotificationException(Exception):
    """Encapsulated the errors that can come back from the data api
    usually in HTTP form
    """

    def __init__(self, base_exception):
        Exception.__init__(self)
        self.base_exception = base_exception

    def __str__(self):
        return str(type(self))

class UnknownNotificationException(NotificationException):
    """This exception doesn't fit into any good category"""

    def __init__(self, base_exception):
        NotificationException.__init__(self, base_exception)

class NotificationNotFoundException(NotificationException):
    """This exception is thrown when a notification can't be found"""

    def __init__(self, base_exception):
        NotificationException.__init__(self, base_exception)

class UnauthorizedNotificationException(NotificationException):
    """This exception is thrown when you are not authorised to send"""

    def __init__(self, base_exception):
        NotificationException.__init__(self, base_exception)

def send(hoist, name, data):
    """Send a notification"""
    url = urlparse.urljoin(NOTIFY_URL, "/".join(["notification", name]))
    print url
    auth_cookie = None
    if hoist.auth:
        auth_cookie = hoist.auth.auth_cookie

    return util.loadUrl(url, hoist.api_key, data,
                        auth_cookie, http_error_to_exception)
