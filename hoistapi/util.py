"""Util functions for hoist api"""

import urllib2
import json

debug = False

def loadUrl(url, api_key, data=None, auth_cookie=None,
            http_error_converter=None, method=None):
    """Load a url and return the json result.
    Ignores the returned cookies"""
    return loadUrlWithCookies(url, api_key, data, auth_cookie,
                              http_error_converter, method)[0]

def loadUrlWithCookies(url, api_key, data=None, auth_cookie=None,
                       http_error_converter=None, method=None):
    """Load a url and return the json result and cookie"""
    if debug:
        print "loadUrl", method, url, api_key, auth_cookie
    req = urllib2.Request(url)
    req.add_header('Authorization', 'HOIST ' + api_key)
    req.add_header('Content-Type', "application/json; charset=UTF-8")
    if method: #override standard http method
        req.get_method = lambda: method
    if auth_cookie:
        req.add_header('Cookie', auth_cookie)
    try:
        if data:
            res = urllib2.urlopen(req, json.dumps(data))
        else:
            res = urllib2.urlopen(req)
    except urllib2.HTTPError as ex:
        if debug:
            print "EX", ex
            print " ".join(ex.hdrs)

        if http_error_converter:
            newex = http_error_converter(ex)
            raise newex
        else:
            raise ex
    if debug:
        print "".join(res.info().headers)
    obj = json.load(res)
    return obj, res.info().get("Set-Cookie")
