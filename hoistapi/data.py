"""DATA

The data end point will allow you to save and retrieve
information for your application.

Disabling anonymous data in the portal settings means
that any request to data will need to have a session
cookie established by a login

GET https://data.hoi.io/{MODEL_NAME}
returns a collection of objects of type MODEL_NAME

GET https://data.hoi.io/{MODEL_NAME}/{id}
returns the single object associated by the model and id supplied

POST https://data.hoi.io/{MODEL_NAME}/{id}

body: {your model}

creates or updates the model with the set id and model type.
"""

import urllib2
import urlparse

from hoistapi import util

DATA_URL = "https://data.hoi.io/"

def http_error_to_exception(http_error):
    if not isinstance(http_error, urllib2.HTTPError):
        return UnknownDataException(http_error)
    elif http_error.code == 409:
        return ConflictDataException(http_error)
    elif http_error.code == 401:
        return UnauthorizedDataException(http_error)
    elif http_error.code == 404:
        return NotFoundException(http_error)
    else:
        return UnknownDataException(http_error)



class DataException(Exception):
    """Encapsulated the errors that can come back from the data api
    usually in HTTP form
    """

    def __init__(self, base_exception):
        Exception.__init__(self)
        self.base_exception = base_exception

    def __str__(self):
        return str(type(self))

class UnknownDataException(DataException):
    """This exception doesn't fit into any good category"""

    def __init__(self, base_exception):
        DataException.__init__(self, base_exception)


class ConflictDataException(DataException):
    """The api returned a HTTP 409 conflict
    usually because the _rev property was not included when updating an object
    """

    def __init__(self, base_exception):
        DataException.__init__(self, base_exception)

class UnauthorizedDataException(DataException):
    """The api returned a HTTP 401 unauthorized
    usually because your API key is incorrect, or and auth object is required
    """

    def __init__(self, base_exception):
        DataException.__init__(self, base_exception)

class NotFoundException(DataException):
    """The api returned a HTTP 404
    usually because you looked for something thats not there
    """

    def __init__(self, base_exception):
        DataException.__init__(self, base_exception)

def convert_key(key):
    """convert a key a string or None if None"""
    if key == None:
        return None
    return str(key)


class HoistModel(object):
    """Creates an object that will handle the data requests for you.

    class data.HoistModel(hoist, model_name)
    Return a new instance of Hoist using the supplied initalised
    Hoist object, if the data has anonymous data turned off then
    you will need to be loged in at the Hosit object level.

    Operates like a dictionary

    votes = HoistModel(hoist,"votes")
    vote = votes["voteid"]
    votes["voteid2"] = {} #new vote object (or one got from hoist)
    del votes["voteid2"]
    for vote in votes: #gets actual objects not keys
      print vote
    len(votes)
    votes.set_object(vote_with_no_id)
    """

    def __init__(self, hoist, model_name):
        self.model_name = model_name
        self.hoist = hoist
        self.__last = None

    #data object interface
    def __getitem__(self, key):
        return self.get_object(key)

    def __setitem__(self, key, value):
        self.__last = self.set_object(value, key)
        return self.__last

    def __delitem__(self, key):
        return self.delete_object(key)

    def keys(self):
        return (o["_id"] for o in self.get_objects())

    def __contains__(self, item):
        raise Exception("Not Implemented")

    def  __iter__(self):
        return (o for o in self.get_objects())

    def __len__(self):
        return len(self.get_objects())

    def last_obj(self):
        """Returns the last object to be set"""
        return self.__last

    def get_objects(self):
        return self.call_data_url()

    def get_object(self, model_id):
        return self.call_data_url(model_id)

    def delete_object(self, model_id):
        return self.call_data_url(model_id, method="DELETE")

    def set_object(self, item, key=None):
        """Sets an object without a key, or grabs it from _id if avaliable"""
        if key == None and "_id" in item:
            key = item["_id"]

        self.__last = self.call_data_url(key, item)
        return self.__last

    def call_data_url(self, model_id=None, data=None, method=None):
        url_parts = [self.model_name]
        if model_id:
            url_parts.append(convert_key(model_id))
        url = urlparse.urljoin(DATA_URL, "/".join(url_parts))

        auth_cookie = None
        if self.hoist.auth:
            auth_cookie = self.hoist.auth.auth_cookie

        return util.loadUrl(url, self.hoist.api_key, data, auth_cookie,
                            http_error_to_exception, method)

#TODO: Missing functions?
#iter(), values(), items(), has_key(), get(), clear(),
#setdefault(), iterkeys(), itervalues(), iteritems(),
#pop(), popitem(), copy(), and update()
