import sys
import argparse
import pprint 
import json
from hoistapi.data import Data

def create_parser():
    parser = argparse.ArgumentParser(description='View / Set Data in hoist')
    subparsers = parser.add_subparsers()
    parser_vi = subparsers.add_parser('view_item', help='view an item in hoist')
    parser_vi.add_argument('api_key', help='The api key to use')
    parser_vi.add_argument('model_name', help='The model to access') 
    parser_vi.add_argument('--ids', nargs="*", help='The id to access')
    parser_vi.set_defaults(func=view_data)

    parser_si = subparsers.add_parser('set_item', help='set an item in hoist')
    parser_si.add_argument('api_key', help='The api key to use')
    parser_si.add_argument('model_name', help='The model to access') 
    parser_si.add_argument('id', help='The id to set')
    group = parser_si.add_mutually_exclusive_group(required=True)    
    group.add_argument('--json', help='Take the json object from the commandline')
    group.add_argument('--file', type=argparse.FileType('r'),  help='Take the json object from a file')
    parser_si.set_defaults(func=set_data)

    parser_di = subparsers.add_parser('delete_item', help='delete an item in hoist')
    parser_di.add_argument('api_key', help='The api key to use')
    parser_di.add_argument('model_name', help='The model to access') 
    parser_di.add_argument('id', help='The id to delete')
    parser_di.set_defaults(func=delete_data)

    return parser

def view_data(args):
    d = Data(args.api_key)
    if not args.ids:
        print json.dumps(d.getObjects(args.model_name), indent=4)
    else:
        for id in args.ids:
            try:
                print json.dumps(d.getObject(args.model_name, id),indent=4) 
            except Exception as ex:
                print "ERROR with id", id
                print ex
                

def set_data(args):
    d = Data(args.api_key)
    j_obj = None
    try:
        if (args.json):
            j_obj = json.loads(args.json)
        else:
            j_obj = json.load(args.file)
    except Exception as ex:
        print "Invalid Json"
        pprint.pprint(ex)
        exit(1)
    
    print json.dumps(d.setObject(args.model_name, args.id, j_obj),indent=4)

def delete_data(args):
    d = Data(args.api_key)
    try:
        print json.dumps(d.deleteObject(args.model_name, args.id),indent=4)
    except Exception as ex:
        print "ERROR with id", args.id
        print ex

if __name__ == '__main__':
    args = create_parser().parse_args()
    args.func(args)



