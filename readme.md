# A Python Package to Access Hoist #

A simple package to access [Hoist](http://hoistapps.com) via python

Usage:

    import hoistapi

    h = hoistapi.Hoist("APIKEY")
	
	h.login("username","password") #login as hoist user
	h.status() #check status of session
	
	votes = h["votes"] #returns the votes model object
	print len(votes)
	for v in votes:
		print v
	print v["existing_id"] #get a specific model
	votes["new_id"] = { "Name": "Andrew", "Date": "2014-01-01" } #set a new model to a specific key
	print votes.last_obj() #return the last set model
	votes.set_object({"Name": "Andrew", "Date": "2014-01-01" }) #set a model where I don't care about the id
	votes.set_object(existing_object) #set model using the _id property returned from Hoist
	del votes["existing_id"] # delete existing data object
	
	
	
